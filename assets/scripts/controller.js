/** By Vinod Chauhan */

// Assignment.json task to fetch data and console it

fetch("./assets/mock/Assignment.json")
    .then(function (res) {
        return res.json();
    })
    .then(function (data) {
        var tksData = data.systemscenario[0].processbranch[0].processflow;
        // console.log(tksData);
        var newArray = new Array;
        tksData.forEach(function (item, index) {
            var inner1Data = item.processscenario
            // console.log('inner1Data', inner1Data, index);
            inner1Data.forEach(function (item, index) {
                var inner2Data = item.componentbranch
                // console.log('inner2Data', inner2Data, index);
                inner2Data.forEach(function (item, index) {
                    var inner3Data = item.component || item.componentbranchcondition
                    if (item.component) {
                        inner3Data.forEach(function (item, index) {
                            newArray.push(item);
                        });
                    }
                    // console.log('inner3Data', inner3Data, index);
                    if (item.componentbranchcondition) {
                        inner3Data.forEach(function (item, index) {
                            var inner4Data = item.componentbranch
                            // console.log('inner4Data', inner4Data, index);
                            inner4Data.forEach(function (item, index) {
                                var inner5Data = item.component
                                // console.log('inner5Data', inner5Data, index);
                                inner5Data.forEach(function (item, index) {
                                    newArray.push(item);
                                });

                            });
                        });
                    }
                });
            });
        });
        console.log('new array with componentbranch/componentbranchcondition', newArray);
    })


var app = angular.module("tksApp", ["ngRoute"]);

app.config(function ($routeProvider) {
    $routeProvider
        .when("/index", {
            templateUrl: '././pages/home.html'
        })
        .when("/job", {
            templateUrl: '././pages/job.html',
            controller: 'MainCtrl'
        })
        .otherwise({
            redirectTo: '/index'
        })
});

//  Injecting Blockly-Div in our view 
app.controller('MainCtrl', function () {
    // Main controller
    Blockly.inject('blocklyDiv',
        {
            toolbox: document.getElementById('toolbox'),
        });
});


// Fibonacci task for recursive time-complexity
/**
    Recursion take exponential time, thus time complexity is T(n) = T(n-1) + T(n-2)
    ref. taken from:- https://www.geeksforgeeks.org/program-for-nth-fibonacci-number/
  */

function FibRecursive(n) {
    if (n < 2) {
        return n
    }
    return FibRecursive(n - 1) + FibRecursive(n - 2)
}
console.log('Fib Recursive of 8 will be:', FibRecursive(8));